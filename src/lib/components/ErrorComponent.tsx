import React from 'react'
import styled from 'styled-components'

export const ErrorComponent: React.FC = () => (
    <ErrorText>
        Oops! Something went bad.
    </ErrorText>
)

const ErrorText = styled.div`
    color: #d34d4d;
    font-size: 40px;
    margin: 70px;
    text-align: center;
`
