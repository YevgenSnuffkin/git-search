import { buildQuery } from 'utils/query'
import { BASE_URL } from './constants'
import { PersistantStorage } from 'lib/localCache'

type SearchParams = {
    q: string
}

export class BaseApiService {
    public static get(params: SearchParams) {
        // here check for cache
        const cached = PersistantStorage.getSearchItem(params.q)

        if (cached) {
            console.log('cached', cached.searchResult)
            return Promise.resolve({
                items: cached.searchResult
            })
        }

        return fetch(`${BASE_URL}${buildQuery(params)}`)
            .then(response => {
                if (!response.ok) {
                    throw response
                }

                return response.json()
            })
    }
}
