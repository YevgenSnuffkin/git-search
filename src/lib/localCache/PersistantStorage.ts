import { GithubRepo } from 'types'

type CachedResults = {
    searchResult: Array<GithubRepo>,
    query: string
}

export class PersistantStorage {
    public static setSearchItem(searchResult: Array<GithubRepo>, query: string) {
        this.setResults({
            searchResult,
            query
        })
    }

    public static getSearchItem(queryToSearch: string) {
        const results = this.getResults()

        return results.find(item => item.query === queryToSearch)
    }

    private static setResults(newResults: CachedResults) {
        localStorage.setItem('github_cache', JSON.stringify([
            ...this.getResults(),
            newResults
        ]))
    }

    private static getResults() {
        const allResults = localStorage.getItem('github_cache')

        if (allResults) {
            return JSON.parse(allResults) as Array<CachedResults>
        }

        return []
    }
}
