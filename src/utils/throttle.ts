type AsyncFn = (params?: any) => Promise<void>

export const throttled = (delay: number, fn: AsyncFn) => {
    let lastCall = 0

    return async (value: string) => {
        const now = new Date().getTime()
        if (now - lastCall < delay) {
            return
        }
        lastCall = now

        return await fn(value)
    }
}
