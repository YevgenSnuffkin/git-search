const compare = (key: never) => (a: number, b: number) => {
    if (a[key] < b[key]) return -1
    if (a[key] > b[key]) return 1

    return 0
}

export const sort = (key: never, input: Array<any>) => [...input].sort(compare(key))
