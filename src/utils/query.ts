export const buildQuery = (params: any) => Object.keys(params)
    .reduce((acc, key, index) => index === 0
        ? acc + encodeURI(`?${key}=${params[key]}`)
        : acc + encodeURI(`&${key}=${params[key]}`)
        , '')

export const setQueryToUrl = (pathname: string) => {
    window.history.replaceState({}, '', pathname)
}
