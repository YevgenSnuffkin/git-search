import { User } from './user'

export type GithubRepo = {
    full_name: string,
    id: number,
    created_at: string,
    stargazers_count: number,
    owner: User,
}
