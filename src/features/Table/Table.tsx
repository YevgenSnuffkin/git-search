import React, { ReactText } from 'react'
import styled from 'styled-components'
import { GithubRepo } from 'types'

const dataColumns = [
    'full_name',
    'id',
    'created_at',
    'stargazers_count'
]

const renderRow = (row: GithubRepo) => {
    const oneRow = dataColumns
        .map(column => {
            const value = row[column  as keyof GithubRepo]

            if(column === 'created_at') {
                return new Date(value as ReactText).toDateString()
            }

            return value
        })
        .map((item, index) => <Td key={index}>{JSON.stringify(item)}</Td>)

    return (
        <tr key={row.id}>
            {oneRow}
        </tr>
    )
}

type TableProps = {
    dataRows: Array<GithubRepo>
}

export const Table: React.FC<TableProps> = ({ dataRows }) => (
    <table>
        <thead>
            <tr>
                {dataColumns.map((column, index) => <th key={index}>{column}</th>)}
            </tr>
        </thead>
        <tbody>
            {dataRows.map(renderRow)}
        </tbody>
    </table>
)

const Td = styled.td`
    border-bottom: 2px solid #e1dede;
    min-width: 190px;
    height: 30px;
    border-right: 2px solid #e1dede;
    padding-left: 3px;
`
