import React from 'react'
import { GithubRepo } from 'types'
import { ErrorComponent } from 'lib/components'
import { Table } from './Table'

type TableContainerProps = {
    hasError: boolean,
    rows: Array<GithubRepo>
}

export const TableContainer: React.FC<TableContainerProps> = ({ hasError, rows }) => hasError ? (
    <ErrorComponent/>
) : (
    <>
        {rows.length > 0 && <Table dataRows={rows}/>}
    </>
)
