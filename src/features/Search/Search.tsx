import React from 'react'
import styled from 'styled-components'
import { throttled } from 'utils/throttle'
import { buildQuery, setQueryToUrl } from 'utils/query'

type SearchProps = {
    onThrottledChange: (params?: any) => Promise<void>
}

export const Search: React.FunctionComponent<SearchProps> = props => {
    const searchThrottled = throttled(1000, props.onThrottledChange)
    const onChange = (value: string) => {
        setQueryToUrl(buildQuery({ q: value }))

        searchThrottled(value)
    }

    return (
        <SearchInput
            placeholder="Search GitHub"
            onChange={({ target }) => onChange(target.value)}
        />
    )
}

const SearchInput = styled.input`
    width: 200px;
    height: 35px;
    border: 3px solid #86f6ff;
    border-radius: 20px;
    font-size: 15px;
    font-family: sans-serif;
    padding: 5px 15px;
`
