import React, { useState, useReducer, useEffect } from 'react'
import styled from 'styled-components'
import { GithubRepo } from 'types'
import { Search } from 'features/Search'
import { TableContainer } from 'features/Table'
import { BaseApiService } from 'lib/api'
import { PersistantStorage } from 'lib/localCache'
import { sort } from 'utils/sort'
import './App.css'

const App: React.FC = () => {
    const [searchResults, setSearchResults] = useState<Array<GithubRepo>>([])
    const [hasError, setError] = useState(false)
    const [isFetching, setFetching] = useState(false)

    useEffect(() => {
        const params = new URLSearchParams(window.location.search)
        const [ q ] = params.getAll('q')

        BaseApiService.get({ q })
            .then(results => {
                setFetching(false)
                setError(false)
                setSearchResults(results.items)
                PersistantStorage.setSearchItem(results.items, q)
            })
            .catch(() => {
                setFetching(false)
                setError(true)
            })       
    }, [])

    const onQuerySubmit = async (q: string) => {
        setFetching(true)

        return BaseApiService.get({ q })
            .then(results => {
                setFetching(false)
                setError(false)
                setSearchResults(results.items)
            })
            .catch(() => {
                setFetching(false)
                setError(true)
            })
    }

    const onSort = (by: never) => {
        const sorted = sort(by, searchResults)

        setSearchResults(sorted)
    }

    const renderButton = (by: string, text: string) => (
        <Button onClick={() => onSort(by as never)}>
            {text}
        </Button>
    )

    return (
        <Container>
            <Search onThrottledChange={onQuerySubmit} />
            <ButtonContainer>
                {renderButton('full_name', 'sort by name')}
                {renderButton('id', 'sort by id')}
                {renderButton('stargazers_count', 'sort by stars')}
            </ButtonContainer>
            {isFetching ? (
                <div>
                    Loading...
                </div>
            ) : (
                <TableContainer
                    rows={searchResults}
                    hasError={hasError}
                />
            )}
        </Container>
    )
}

const Container = styled.div`
    padding: 20px;
    display: flex;
    flex-direction: column;
    align-items: center;
`

const Button = styled.button`
    height: 35px;
    font-size: 18px;
    text-transform: uppercase;
    width: 200px;
    background-color: #fafafa;
    border: 1px solid #86f6ff;
    border-radius: 17px;
    margin: 5px;
`

const ButtonContainer = styled.div`
    display: flex;
`

export default App
